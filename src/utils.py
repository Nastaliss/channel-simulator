def teb(input, output):
    eb = 0
    for i in range(len(input)):
        if input[i] != output[i]:
            eb += 1
    return eb / len(input)
