import random
from functools import reduce
from math import pi, sin

from scipy import signal

SAMPLE_RATE = 100
SIN_PER_BIT = 10


class Signal(object):
    def __init__(self, bits_per_frame, channel_coding, noise_sigma):
        self.data = list(map(lambda index: random.randint(
            0, 1), range(0, bits_per_frame)))
        self.channel_coding = channel_coding
        self.noise_sigma = noise_sigma

    def apply_channel_coding(self):
        self.data = self.channel_coding.code(self.data)
        self.data = list(reduce(lambda previous_value, next_value: [
            *previous_value, int(format(next_value, '02b')[0]), int(format(next_value, '02b')[1])], self.data, []))
        return self.data

    def apply_ask(self):
        modulated_data = []
        for signal_bit in self.data:
            for i in range(0, SAMPLE_RATE):
                if(signal_bit == 0):
                    modulated_data.append(0)
                else:
                    modulated_data.append(
                        sin(SIN_PER_BIT * 2 * pi * i / SAMPLE_RATE))

        self.data = modulated_data
        return self.data

    def apply_noise(self):
        self.data = list(
            map(lambda bit: bit + random.gauss(0, self.noise_sigma), self.data))
        return self.data

    def apply_pre_decision(self):
        self.data = [x * sin(SIN_PER_BIT * 2 * pi * index / SAMPLE_RATE)
                     for index, x in enumerate(self.data)]
        self.data = signal.lfilter(
            *signal.butter(1, 5/50, btype='low'), self.data)
        return self.data

    def apply_decision(self):
        print(len(self.data)/(2*SAMPLE_RATE))
        demodulated_output = []
        for window_index in range(0, int(len(self.data)/(SAMPLE_RATE))):
            window_start = window_index * SAMPLE_RATE
            window_end = window_index * SAMPLE_RATE + SAMPLE_RATE - 1
            print(window_start, window_end)
            window_average = reduce(lambda previous, current: previous +
                                    current, self.data[window_start: window_end]) / SAMPLE_RATE
            print(window_average)
            if window_average < .25:
                demodulated_output.append(0)
            else:
                demodulated_output.append(1)
        self.data = demodulated_output
        return self.data

    def apply_channel_decoding(self):
        self.data = list(map(
            lambda index: self.data[index] * 2 + self.data[index+1], range(0, len(self.data))[0::2]))
        self.data = self.channel_coding.decode(self.data)
        return self.data
