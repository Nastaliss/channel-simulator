from abc import abstractmethod


class ChannelCoding(object):
    """
    Interface for Channel coding classes
    """
    @abstractmethod
    def code(self, signal):
        raise NotImplementedError()

    @abstractmethod
    def decode(self, signal):
        raise NotImplementedError()
