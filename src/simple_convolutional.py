from .channel_coding import ChannelCoding

SHIFT_REGISTER_LENGTH = 3
OUTPUT_STREAM_1 = [1, 1, 1]
OUTPUT_STREAM_2 = [1, 0, 1]


class SimpleConvolutional(ChannelCoding):

    def __init__(self):
        pass

    def code(self, signal):
        shift_register = [0] * SHIFT_REGISTER_LENGTH
        output = []
        for i in range(0, len(signal)):
            shift_register = [signal[i], *shift_register[0:-1]]
            output.append(self.calculate_output(shift_register))

        return output

    def calculate_output(self, shift_register):
        output_stream_1_xor_counter = 0
        output_stream_2_xor_counter = 0
        for bit_index, shift_register_bit in enumerate(shift_register):

            if (OUTPUT_STREAM_1[bit_index] and shift_register_bit):
                output_stream_1_xor_counter += 1

            if (OUTPUT_STREAM_2[bit_index] and shift_register_bit):
                output_stream_2_xor_counter += 1

        return int(f"{output_stream_1_xor_counter % 2}{output_stream_2_xor_counter % 2}", 2)

    def decode(self, signal):
        self.shortest_distance = float('inf')
        self.find_all_paths(signal)
        return self.shortest_distance_inputs

    def find_all_paths(self, received_signal, current_state='a', bit_index=0, calculated_inputs=[], distance=0):
        # Exit strategy
        if bit_index == len(received_signal):
            if distance < self.shortest_distance:
                self.shortest_distance = distance
                self.shortest_distance_inputs = calculated_inputs
            return

        for input_bit, available_branch in enumerate(self.get_grid()[current_state]):
            self.find_all_paths(received_signal, available_branch['state'], bit_index+1,
                                [*calculated_inputs, input_bit], distance + self.calculate_distance(received_signal[bit_index], available_branch['out']))

    def get_grid(self):
        return {
            'a': [{'state': 'a', 'out': 0b00}, {'state': 'b', 'out': 0b11}],
            'b': [{'state': 'd', 'out': 0b10}, {'state': 'c', 'out': 0b01}],
            'c': [{'state': 'd', 'out': 0b01}, {'state': 'c', 'out': 0b10}],
            'd': [{'state': 'a', 'out': 0b11}, {'state': 'b', 'out': 0b00}],
        }

    def calculate_distance(self, symbol1, symbol2):
        differences = 0
        for bit in bin(symbol1 ^ symbol2)[2:]:
            differences += int(bit)
        return differences
