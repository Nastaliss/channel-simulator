import random
from copy import deepcopy

import matplotlib.pyplot as plot

from src.signal import Signal
from src.simple_convolutional import SimpleConvolutional as CodeFunction
from src.utils import teb

signal = Signal(100, CodeFunction(), noise_sigma=3)
input_signal = deepcopy(signal.data)
channel_coding = deepcopy(signal.apply_channel_coding())
bpsk = deepcopy(signal.apply_ask())
noise = deepcopy(signal.apply_noise())
pre_decision = deepcopy(signal.apply_pre_decision())
decision = deepcopy(signal.apply_decision())
channel_decoding = deepcopy(signal.apply_channel_decoding())


# ─── DISPLAY ────────────────────────────────────────────────────────────────────
print('DISPLAYING')
plots = [
    {'data': input_signal, 'name': 'Input data', 'type': 'discrete'},
    {'data': channel_coding, 'name': 'channel coding', 'type': 'discrete'},
    {'data': bpsk, 'name': 'ask coding', 'type': 'discrete'},
    {'data': noise, 'name': 'Noisy', 'type': 'discrete'},
    {'data': pre_decision, 'name': 'pre-ecision', 'type': 'discrete'},
    {'data': decision,
        'name': f'Decision TEB:{teb(decision, channel_coding)}', 'type': 'discrete'},
    {'data': channel_decoding,
        'name': f"channel decoding TEB:{teb(input_signal, channel_decoding)}", 'type': 'discrete'},
]

for plot_index, sig_plot in enumerate(plots):
    plot.subplot(len(plots), 1, plot_index + 1)
    plot.title(sig_plot['name'])
    plot.stem(sig_plot['data'], use_line_collection=True)

plot.show()
